# Spanish translations for plasma-browser-integration-host.po package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the plasma-browser-integration package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-browser-integration-host\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-29 00:19+0000\n"
"PO-Revision-Date: 2021-04-19 12:36+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.03.90\n"

#: downloadjob.cpp:134
#, kde-format
msgid ""
"This type of file can harm your computer. If you want to keep it, accept "
"this download from the browser window."
msgstr ""
"Este tipo de archivo puede dañar su equipo. Si desea conservarlo, acepte "
"esta descarga en la ventana del navegador."

#: downloadjob.cpp:231
#, kde-format
msgid "Access denied."
msgstr "Acceso denegado."

#: downloadjob.cpp:232
#, kde-format
msgid "Insufficient free space."
msgstr "Espacio libre insuficiente."

#: downloadjob.cpp:233
#, kde-format
msgid "The file name you have chosen is too long."
msgstr "El nombre de archivo que ha elegido es demasiado largo."

#: downloadjob.cpp:234
#, kde-format
msgid "The file is too large to be downloaded."
msgstr "El archivo es demasiado grande para descargarlo."

#: downloadjob.cpp:236
#, kde-format
msgid "The file possibly contains malicious contents."
msgstr "Es posible que el archivo tenga contenido malicioso."

#: downloadjob.cpp:237
#, kde-format
msgid "A temporary error has occurred. Please try again later."
msgstr ""
"Ha ocurrido un error temporal. Por favor, vuelva a intentarlo más tarde."

#: downloadjob.cpp:239
#, kde-format
msgid "A network error has occurred."
msgstr "Ha ocurrido un error de red."

#: downloadjob.cpp:240
#, kde-format
msgid "The network operation timed out."
msgstr "Se ha agotado el tiempo de la operación de red."

#: downloadjob.cpp:241
#, kde-format
msgid "The network connection has been lost."
msgstr "Se ha perdido la conexión de red."

#: downloadjob.cpp:242
#, kde-format
msgid "The server is no longer reachable."
msgstr "El servidor ya no está disponible."

#: downloadjob.cpp:244
#, kde-format
msgid "A server error has occurred."
msgstr "Ha ocurrido un error del servidor."

#: downloadjob.cpp:248
#, kde-format
msgid "The server does not have the requested data."
msgstr "El servidor no posee los datos solicitados."

#: downloadjob.cpp:250
#, kde-format
msgid "The browser application closed unexpectedly."
msgstr "La aplicación de navegación se ha cerrado de forma no esperada."

#: downloadjob.cpp:256
#, kde-format
msgid "An unknown error occurred while downloading."
msgstr "Ha ocurrido un error desconocido durante la descarga."

#: downloadjob.cpp:294
#, kde-format
msgctxt "Job heading, like 'Copying'"
msgid "Downloading"
msgstr "Descargando"

#: downloadjob.cpp:295
#, kde-format
msgctxt "The URL being downloaded"
msgid "Source"
msgstr "Origen"

#: downloadjob.cpp:296
#, kde-format
msgctxt "The location being downloaded to"
msgid "Destination"
msgstr "Destino"

#: historyrunnerplugin.cpp:108
#, kde-format
msgctxt "Dummy search result"
msgid "Additional permissions are required"
msgstr "Se necesitan permisos adicionales"

#: tabsrunnerplugin.cpp:38
#, kde-format
msgid "Mute Tab"
msgstr "Silenciar pestaña"

#: tabsrunnerplugin.cpp:43
#, kde-format
msgid "Unmute Tab"
msgstr "Restaurar sonido en pestaña"
